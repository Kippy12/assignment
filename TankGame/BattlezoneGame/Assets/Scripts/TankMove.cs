﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMove : MonoBehaviour
{
    [SerializeField] private string forwardInputName;
    [SerializeField] private string strafeInputName;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;
    [SerializeField] private KeyCode runKey;

    // Jump 
    [SerializeField] private KeyCode jumpKey;
    [SerializeField] private float jumpMultiplier;
    [SerializeField] private AnimationCurve jumpFalloff;

    private CharacterController charController;
    private bool isRunning;
    private bool isJumping;

    // Bounce / Jitter elimination
    [SerializeField] private float slopeForceRayLength;
    [SerializeField] private float slopeForce;

    private void Awake()
    {
        charController = GetComponent<CharacterController>();
        isRunning = false;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    bool onSlope()
    {
        if (!isJumping)
        {
            // We're NOT jumping...

            RaycastHit groundHit;

            // Can we "see" the ground?...
            if (Physics.Raycast(
                transform.position,
                Vector3.down,
                out groundHit,
                charController.height / 2.0f * slopeForceRayLength
                ))
            {
                // Yes - ray hit something - now check if the surface we hit is flat or not...
                if (groundHit.normal != Vector3.up)
                {
                    // We're on a slope
                    return true;
                }
            }
        }

        return false;
    }

    void HandlePlayerMove()
    {
        float speed;

        if (Input.GetKeyDown(runKey))
        {
            isRunning = true;
        }
        else if (Input.GetKeyUp(runKey))
        {
            isRunning = false;
        }

        if (isRunning)
        {
            speed = runSpeed;
        }
        else
        {
            speed = walkSpeed;
        }

        float forwardInput = Input.GetAxis(forwardInputName);
        float strafeInput = Input.GetAxis(strafeInputName);

        Vector3 forwardMovement = transform.forward * forwardInput;
        Vector3 strafeMovement = transform.right * strafeInput;

        Vector3 actualMovement = Vector3.ClampMagnitude(forwardMovement + strafeMovement, 1.0f) * speed;

        charController.SimpleMove(actualMovement);

        // Check if moving on a slope
        if ((forwardInput != 0 || strafeInput != 0) && onSlope())
        {
            // Yes moving, not jumping AND on angled surface - so apply force
            Vector3 downForce = Vector3.down * (charController.height / 2.0f) * slopeForce * Time.deltaTime;
            charController.Move(downForce);
        }


        // Jump - check if we can jump
        if (Input.GetKeyDown(jumpKey) && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }
    // Update is called once per frame
    void Update()
    {
        HandlePlayerMove();

        
    }


    IEnumerator JumpEvent()
    {
        float timeInAir = 0.0f;

        do
        {
            // Jump logic (for 1 step of the jump)
            float jumpForce = jumpFalloff.Evaluate(timeInAir);
            Vector3 jumpDirection = Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime;
            charController.Move(jumpDirection);
            timeInAir += Time.deltaTime;

            yield return null;

        } while (!charController.isGrounded && charController.collisionFlags != CollisionFlags.Above);

        isJumping = false;
    }

    public bool SpeedBoost = false;
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SpeedBoost"))
        {
            Pickup();
        }
    }

    public void Pickup()
    {

        Debug.Log("Hello Humans");
        SpeedBoost = true;
        walkSpeed = -12;
        runSpeed = -24;
    }

}

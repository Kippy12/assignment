﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftController : MonoBehaviour
{
    [SerializeField] private Transform lowerCoord;
    [SerializeField] private Transform upperCoord;
    [SerializeField] private AnimationCurve liftControlCurve;

    private float timeElapsed = .0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float t = liftControlCurve.Evaluate(timeElapsed);

        Vector3 pos = lowerCoord.position * (1.0f - t) + upperCoord.position * t;
        transform.position = pos;

        timeElapsed += Time.deltaTime;
    }
}